let total=0;
let assumeString="0";
let operator;
let addingOutput=document.querySelector('.valuecontainer');

function buttonClick(value){
  if(isNaN(parseInt(value))){
    sym(value)
  }else{
    num(value)
  }
  addValue();
}

function num(value){
  if(assumeString==="0"){
    assumeString=value
  }else{
    assumeString+=value
  }
}

function operationsasm(value){
  if(assumeString==="0"){
    return;
  }
  const integer=parseInt(assumeString);
  if(total===0){
    total=integer;
  }else{
    doingOperations(integer)
  }
  operator=value;
  assumeString="0";  
}


function doingOperations(integer){
  if(operator==="+"){
    total=total+integer;
  }else if(operator==="-"){
    total=total-integer;
  }else if(operator==="*"){
    total=total*integer;
  }else if(operator==="÷"){
    total=total/integer;
  }
}

function sym(value){
  if(value === "C"){
    assumeString="0";
    total=0;
  }else if(value ==="←"){
    if(assumeString.length===1){
      assumeString="0";
    }else{
      assumeString=assumeString.substring(0,assumeString.length-1)
    }
  }
  else if(value === "+"){
    operationsasm(value);
  }else if(value === "-"){
    operationsasm(value);
  }else if(value === "÷"){
    operationsasm(value);
  }else if(value === "*"){
    operationsasm(value);
  }else if(value === "="){
    if(operator===null){
      return
    }
    doingOperations (parseInt(assumeString));
    operator=null;
    assumeString=""+total;
    total=0;
  }
}

function addValue(){
  addingOutput.innerText=assumeString;
}

function mainFunction(){
  document.querySelector(".btns").addEventListener("click",function(event){
    buttonClick(event.target.innerText)
  })
}

mainFunction()