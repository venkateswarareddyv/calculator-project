# CALCULATOR PROJECT

This project is a replica of calculator. It helps to do some basic maths operations.

## Used technologies
---
<ul>
    <li>Html is used to made the structure how to appear.</li>
    <li>CSS is used to add styles.</li>
    <li>JavaScript and DOM manipulations is used to make interactive.</li>
</ul>

## CHALLENGES
---
<ul>
    <li>To create boxes and align them together.</li>
    <li>Taking input from buttons based on user interaction.</li>
    <li>Display them in a proper manner.</li>
</ul>

## Changes which I want to do in future
---
<ul>
    <li>To add some more operations.</li>
</ul>

## Visit this site to do basic calculations
---

URL : https://elaborate-dieffenbachia-0d8d9f.netlify.app